﻿using System;
using System.Threading.Tasks;
using Common.Context;
using Common.Messages;
using Common.Models;
using Common.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Frontend.Controllers
{
    public class ApiController : ControllerBase
    {
        private readonly IContextFactory contextFactory;

        public ApiController(IContextFactory contextFactory)
        {
            this.contextFactory = contextFactory;
        }

        [HttpGet]
        public async Task<IActionResult> ServiceStat()
        {
            using (var db = contextFactory.Get())
            {
                var result = await db.Services.ToListAsync();
                return new JsonResult(new { result });
            }
        }

        [HttpPost]
        public async Task<IActionResult> UpdateStat([FromBody] State data)
        {
            var host = HttpContext.Connection.RemoteIpAddress.ToString();

            using (var db = contextFactory.Get())
            {
                var result = await db.Services.FirstOrDefaultAsync(x => x.Host == host);
                if (result == null)
                {
                    result = new Service
                    {
                        Host = host,
                        CurrentLoad = data.CurrentLoad,
                        DateLast = DateTime.Now,
                    };
                    await db.Services.AddAsync(result);
                }
                else
                {
                    result.CurrentLoad = data.CurrentLoad;
                    result.DateLast = DateTime.Now;
                }

                await db.SaveChangesAsync();
            }

            return new JsonResult(new StateResult());
        }
    }
}