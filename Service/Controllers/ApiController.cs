﻿using Common.Messages;
using Microsoft.AspNetCore.Mvc;

namespace Service.Controllers
{
    public class ApiController : ControllerBase
    {
        public IActionResult Manage()
        {
            return new JsonResult(new CommansResult { Result = 1 });
        }
    }
}