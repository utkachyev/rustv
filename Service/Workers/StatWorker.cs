﻿using Common.Messages;
using Common.Services.Interfaces;
using System.Threading;
using System.Threading.Tasks;

namespace Service.Workers
{
    public class StatWorker
    {
        CancellationTokenSource cancellationTokenSource = null;
        object syncObject = new object();
        private readonly IHttpService httpService;

        public StatWorker(IHttpService httpService)
        {
            cancellationTokenSource = new CancellationTokenSource();

            Task.Factory.StartNew(async () => await WorkerProc(cancellationTokenSource.Token), TaskCreationOptions.LongRunning);
            this.httpService = httpService;
        }

        public void Stop()
        {
            if (cancellationTokenSource != null)
            {
                lock (syncObject)
                {
                    if (cancellationTokenSource != null)
                    {
                        cancellationTokenSource.Cancel();
                        cancellationTokenSource = null;
                    }
                }
            }
        }

        ~StatWorker()
        {
            Stop();
        }

        protected async Task WorkerProc(CancellationToken ct)
        {
            while (true)
            {
                ct.ThrowIfCancellationRequested();

                try
                {
                    await httpService.Post<StateResult>("/api/UpdateStat", new State
                    {
                        CurrentLoad = 0
                    });
                }
                catch
                {
                    // igmored
                }

                await Task.Delay(5000);
            }
        }
    }
}