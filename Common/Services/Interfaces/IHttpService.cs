﻿using System.Threading;
using System.Threading.Tasks;

namespace Common.Services.Interfaces
{
    public interface IHttpService
    {
        Task<TEntity> Post<TEntity>(string uri, object postData, CancellationToken token = default(CancellationToken));
    }
}