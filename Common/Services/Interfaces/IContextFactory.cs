﻿using Common.Context;

namespace Common.Services.Interfaces
{
    public interface IContextFactory
    {
        RusTvContext Get();
    }
}