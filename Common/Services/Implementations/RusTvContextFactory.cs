﻿using Common.Context;
using Common.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Common.Services.Implementations
{
    public class RusTvContextFactory : IContextFactory
    {
        private readonly IServiceProvider serviceProvider;

        public RusTvContextFactory(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public RusTvContext Get()
        {
            return serviceProvider.GetService(typeof(RusTvContext)) as RusTvContext;
        }
    }
}
