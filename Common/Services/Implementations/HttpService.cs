﻿using Common.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Common.Services.Implementations
{
    public class HttpService : IHttpService
    {
        HttpClient _httpClient = new HttpClient();
        string _uriBase;

        public HttpService(IConfiguration configuration)
        {
            _uriBase = configuration.GetValue<string>("FrontendUri");
        }

        public async Task<TEntity> Post<TEntity>(string uri, object postData, CancellationToken token = default(CancellationToken))
        {
            string requestUri = _uriBase + uri;
            string requestData = JsonConvert.SerializeObject(postData, DefaultSerialization.SerializerSettings);

            return await PostRequest<TEntity>(requestUri, requestData, token);
        }

        private async Task<TEntity> PostRequest<TEntity>(string requestUri, string requestData, CancellationToken token = default(CancellationToken))
        {
            var requestJson = string.Empty;
            
            TEntity result = default(TEntity);

            using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, new Uri(requestUri)))
            {
                httpRequestMessage.Headers.Add("Accept-Encoding", "gzip");

                httpRequestMessage.Content = new StringContent(
                    requestData,
                    Encoding.UTF8,
                    "application/json"
                    );

                using (var httpResponseMessage = await _httpClient.SendAsync(httpRequestMessage, HttpCompletionOption.ResponseHeadersRead))
                {
                    httpResponseMessage.EnsureSuccessStatusCode();
                    using (var content = httpResponseMessage.Content)
                    {
                        if (content.Headers.ContentEncoding.Any(c => c.ToUpper() == "GZIP"))
                        {
                            var response = await content.ReadAsByteArrayAsync();

                            using (var msi = new MemoryStream(response))
                            using (var mso = new MemoryStream())
                            using (var gs = new GZipStream(msi, CompressionMode.Decompress))
                            {
                                CopyTo(gs, mso);

                                var unzipResult = Encoding.UTF8.GetString(mso.ToArray());

                                result = JsonConvert.DeserializeObject<TEntity>(unzipResult, DefaultSerialization.SerializerSettings);
                            }
                        }
                        else
                        {
                            var response = await content.ReadAsStringAsync();
                            result = JsonConvert.DeserializeObject<TEntity>(response, DefaultSerialization.SerializerSettings);
                        }
                    }
                }

            }

            return result;
        }

        private static void CopyTo(Stream src, Stream dest)
        {
            byte[] bytes = new byte[4096];

            int cnt;

            while ((cnt = src.Read(bytes, 0, bytes.Length)) != 0)
            {
                dest.Write(bytes, 0, cnt);
            }
        }
    }
}
