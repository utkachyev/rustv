﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Common.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

internal static class ModelBuilderExtensions
{
    public static void AddConfiguration<TEntityConfiguration>(this ModelBuilder modelBuilder)
        where TEntityConfiguration : IEntityConfiguration, new()
    {
        var config = new TEntityConfiguration();

        config.Prepare(modelBuilder);
        config.Build();
    }
}

internal interface IEntityConfiguration
{
    void Prepare(ModelBuilder builder);

    void Build();
}

internal abstract class EntityConfiguration<T> : IEntityConfiguration where T : class, new()
{
    protected EntityTypeBuilder<T> Builder { get; set; }

    public void Prepare(ModelBuilder builder)
    {
        Builder = builder.Entity<T>();
    }

    protected PropertyBuilder<TProperty> Property<TProperty>(Expression<Func<T, TProperty>> expr)
    {
        return Builder.Property(expr);
    }

    protected KeyBuilder HasKey(Expression<Func<T, object>> expr)
    {
        return Builder.HasKey(expr);
    }

    protected CollectionNavigationBuilder<T, TRelated> HasMany<TRelated>(Expression<Func<T, IEnumerable<TRelated>>> expr) where TRelated : class
    {
        return Builder.HasMany(expr);
    }

    protected ReferenceNavigationBuilder<T, TRelated> HasOne<TRelated>(Expression<Func<T, TRelated>> expr) where TRelated : class
    {
        return Builder.HasOne(expr);
    }

    protected EntityTypeBuilder<T> ToTable(string tableName, string schemaName = null)
    {
        return Builder.ToTable(tableName, schemaName);
    }

    protected IndexBuilder HasIndex(Expression<Func<T, object>> expr)
    {
        return Builder.HasIndex(expr);
    }

    public abstract void Build();
}

public static class SchemaConstants
{
    public const string DefaultSchemaName = "RT";
}

internal class LiveStreamEntityConfiguration : EntityConfiguration<LiveStream>
{
    public override void Build()
    {
        HasKey(s => s.Id);

        Property(s => s.Name).IsRequired().HasMaxLength(200);
    }
}

internal class ServiceEntityConfiguration : EntityConfiguration<Service>
{
    public override void Build()
    {
        HasKey(s => s.Id);

        Property(s => s.Host).IsRequired().HasMaxLength(200);

        Property(s => s.CurrentLoad).IsRequired().HasDefaultValue(0);
        
        Property(s => s.DateLast).IsRequired();
        HasIndex(s => s.DateLast);
    }
}