﻿using Common.Models;
using Microsoft.EntityFrameworkCore;

namespace Common.Context
{
    public class RusTvContext : DbContext
    {
        public RusTvContext(DbContextOptions<RusTvContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema(SchemaConstants.DefaultSchemaName);

            modelBuilder.AddConfiguration<LiveStreamEntityConfiguration>();

            modelBuilder.ForSqlServerUseIdentityColumns();
        }

        public DbSet<LiveStream> LiveStreams { get; private set; }
        public DbSet<Service> Services { get; private set; }
    }
}