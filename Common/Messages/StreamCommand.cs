﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Messages
{
    public enum OpCode
    {
        Start,
        Stop
    }

    public class StreamCommand
    {
        public OpCode OpCode { get; set; }

        public string StreamName { get; set; }
    }
}
