﻿using System;

namespace Common.Models
{
    public class Service
    {
        public Service()
        {
            CurrentLoad = 0;
            DateLast = DateTime.Now;
        }

        public int Id { get; set; }

        public int CurrentLoad { get; set; }

        public string Host { get; set; }

        public DateTime DateLast { get; set; }
    }
}
