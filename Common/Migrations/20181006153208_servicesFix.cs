﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Common.Migrations
{
    public partial class servicesFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Host",
                schema: "RT",
                table: "Services",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Host",
                schema: "RT",
                table: "Services",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
